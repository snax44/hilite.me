Forked from [https://github.com/alexkay/hilite.me](https://github.com/alexkay/hilite.me)  

Thanks to:  

- Author: [Alexander Kojevnikov](https://github.com/alexkay)  
- Contributor: [Reiner Rottmann](https://github.com/rrottmann) for the python3 compatibility  

# hilite.me

[hilite.me](http://hilite.me/) is a small webapp that converts your code
snippets into pretty-printed HTML format, easily embeddable into blog posts and
websites.

## Install

```bash
apt install python3-pip python3-venv apache2 libapache2-mod-wsgi-py3

cd /opt
git clone https://gitlab.com/snax44/hilite.me.git
cd hilite.me
python3 -m venv env
source env/bin/activate
pip install pip --upgrade
pip install -r requirement.txt

cp apache.example /etc/apache2/sites-available/hilite.me.conf
a2ensite hilite.me
systemctl reload apache2
```
