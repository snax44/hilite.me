#!/usr/bin/env python

import sys
#import logging
 
sys.path.insert(0, '/opt/hilite.me')
sys.path.insert(0, '/opt/hilite.me/env/lib/python3.11/site-packages/')
 
# Set up logging
#logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)
 
# Import and run the Flask app
from main import app as application
